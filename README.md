# Scared notebooks repository

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/eshard%2Fscared-notebooks/master)

eShard's repository with tutorial notebooks for the making side channel attacks using Scared library.

## Scared library

The main requirement for running the notebooks is to install __Scared__.

See [Scared repository](https://gitlab.com/eshard/scared) for more information about the Scared side-channel library.

You can install Scared with __pip__:

```bash
$ pip install scared
```

or __conda__

```bash
$ conda install -c eshard scared
```

## Try the notebooks online with Binder

The notebooks from the repository can easily be executed in an online Jupyter environment using __Binder__. 

Click here [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/eshard%2Fscared-notebooks/master)
 to launch a Binder instance with the Scared notebooks and library already integrated.









