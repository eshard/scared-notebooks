{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='../images/eshard.png' style='width: 180px; float:right;'>\n",
    "\n",
    "# How to build an .ETS dataset\n",
    "\n",
    "---\n",
    "\n",
    "* **Scared library notebooks**\n",
    "* Classification: __PUBLIC__\n",
    "* _CATEGORY_: How-to\n",
    "\n",
    "In this notebook we present how to create an `.ets` dataset to store side-channel traces and the corresponding metadata on disk."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prerequisites\n",
    "----\n",
    "\n",
    "First, we will download a `numpy` dataset from the [CHES 2016 CTF](http://ctf.newae.com/flags/) server and extract it. If not already installed, install `wget` and `py7zr` libraries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install wget\n",
    "!pip install py7zr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We download the StageGate #1 dataset into the local `./traces` folder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "if not os.path.exists('./traces/'):\n",
    "    os.makedirs('./traces/')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import wget\n",
    "import sys\n",
    "\n",
    "def bar_progress(current, total, width=80):\n",
    "    progress_message = \"Downloading: %d%% [%d / %d] bytes\" % (current / total * 100, current, total)\n",
    "    sys.stdout.write(\"\\r\" + progress_message)\n",
    "    sys.stdout.flush()\n",
    "\n",
    "url = 'http://ctf.newae.com/media/trace/traces57.7z'\n",
    "\n",
    "if not os.path.exists('./traces/traces57.7z'):\n",
    "    wget.download(url, './traces/traces57.7z', bar=bar_progress)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then extract the set of traces into the same folder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import py7zr\n",
    "\n",
    "if not os.path.exists('./traces/traces_capdir57'):\n",
    "    archive = py7zr.SevenZipFile('./traces/traces57.7z', mode='r')\n",
    "    archive.extractall(path=\"./traces/\")\n",
    "    archive.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The challenge dataset is stored as three `.npy` files:\n",
    "\n",
    "- `textin.npy` stores the plaintexts used for the encryptions.\n",
    "- `textout.npy` stores the ciphertexts.\n",
    "- `traces.npy` stores the side-channel traces.\n",
    "\n",
    "In such a dataset, traces and metadata are stored in separate files. With an `.ets` file, one can group traces and metadata in a single file stored on disk. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## From a `THS` instance\n",
    "----\n",
    "\n",
    "A first way to create an `.ets` file, it to create it from a `TraceHeaderSet` instance. The `TraceHeaderSet` is an abstract reader API used to read datasets from multiple sources.\n",
    "\n",
    "Here, as the data is stored as `numpy` arrays, we can first load it in memory, and create a `THS` instance corresponding to this data stored in RAM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "plaintext = np.load('./traces/traces_capdir57/secretfixed_rand/secretfixed_rand_P57_data/traces/2016.06.01-09.50.28_textin.npy')\n",
    "ciphertext = np.load('./traces/traces_capdir57/secretfixed_rand/secretfixed_rand_P57_data/traces/2016.06.01-09.50.28_textout.npy')\n",
    "traces = np.load('./traces/traces_capdir57/secretfixed_rand/secretfixed_rand_P57_data/traces/2016.06.01-09.50.28_traces.npy')\n",
    "\n",
    "traces = traces.astype('float32')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import estraces\n",
    "\n",
    "ths = estraces.read_ths_from_ram(samples=traces, **{'plaintext': plaintext, 'ciphertext': ciphertext})\n",
    "\n",
    "print(f'ths contains {len(ths)} traces')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From a `THS` instance, we can then store the traces and metadata as an `.ets` file on the disk as follows: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "writer = estraces.ETSWriter('./traces/stagegate1.ets', overwrite=True)\n",
    "writer.add_trace_header_set(ths)\n",
    "writer.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the dataset is saved on disk, it is possible to open it as a `THS` instance as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ths = estraces.read_ths_from_ets_file('./traces/stagegate1.ets')\n",
    "\n",
    "print(ths.samples.shape)\n",
    "print(ths.plaintext.shape)\n",
    "print(ths.ciphertext.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Batch by batch\n",
    "----\n",
    "\n",
    "It is also possible to manually write data in the `.ets` file batch by batch as in the example below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_batches = 10\n",
    "batch_size = len(traces) // n_batches\n",
    "\n",
    "writer = estraces.ETSWriter('./traces/stagegate1.ets', overwrite=True)\n",
    "\n",
    "for idx in range(0, len(traces), batch_size):\n",
    "    writer.write_samples(traces[idx:idx + batch_size])\n",
    "    writer.write_metadata(key='plaintext', value=plaintext[idx:idx + batch_size])\n",
    "    writer.write_metadata(key='ciphertext', value=ciphertext[idx:idx + batch_size])\n",
    "\n",
    "writer.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compressed `.ets`\n",
    "----\n",
    "\n",
    "It is also possible to compress `.ets` files to save space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "estraces.compress_ets(\"./traces/stagegate1.ets\", \"./traces/compress_stagegate1.ets\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -lah traces | grep \".ets\""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
