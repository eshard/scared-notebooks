{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='../images/eshard.png' style='width: 180px; float:right;'>\n",
    "\n",
    "# How to implement custom reverse selection functions\n",
    "\n",
    "---\n",
    "\n",
    "* **Scared library notebooks**\n",
    "* Classification: __PUBLIC__\n",
    "* _CATEGORY_: How-to\n",
    "\n",
    "In this notebook we present how to implement custom reverse selection functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scared"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The dataset\n",
    "----\n",
    "\n",
    "In this example, we will use the dataset from the StageGate #1 challenge of the [CHES 2016 CTF](http://ctf.newae.com/flags/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ths = scared.traces.read_ths_from_ets_file('./../traces/stagegate1.ets')\n",
    "\n",
    "print(len(ths))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dataset is composed of 1,000 traces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 1: Delta between Sbox input and output\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A reverse selection function takes as input arguments corresponding to some of the dataset metadata fields and must return a `numpy` array of shape `[nb traces x nb bytes]`, where:\n",
    "\n",
    "- `nb traces` corresponds to the number of traces in the dataset.\n",
    "- `nb bytes` corresponds to the number of bytes in the targeted data.\n",
    "\n",
    "To implement a reverse selection selection targeting the delta between the Sbox input and output, one needs to:\n",
    "- compute the Sbox input values.\n",
    "- compute the Sbox output values.\n",
    "- Xor inputs with outputs and return the values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "@scared.reverse_selection_function\n",
    "def delta_sbox_in_sbox_out(plaintext, key):\n",
    "\n",
    "    # Compute Sbox input values\n",
    "    in_sbox = scared.aes.encrypt(plaintext, key, at_round=0, after_step=scared.aes.Steps.ADD_ROUND_KEY)\n",
    "\n",
    "    # Compute Sbox output values\n",
    "    out_sbox = scared.aes.encrypt(plaintext, key, at_round=1, after_step=scared.aes.Steps.SUB_BYTES)\n",
    "\n",
    "    # Xor inputs with outputs and return the result\n",
    "    return np.bitwise_xor(in_sbox, out_sbox)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once defined, the reverse selection function can be used to compute the targeted values from the `THS` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = delta_sbox_in_sbox_out(**ths.metadatas)\n",
    "\n",
    "print(values)\n",
    "print(values.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function can be passed to other `scared` APIs, for instance to the reverse analysis API to perform reverse analysis on the data defined by the function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 2: Custom `MixColumns` operation with `xtime`\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When looking at the source code of the StageGate #1 implementation, one can see that the `MixColumns` operation is implemented as follows:\n",
    "\n",
    "```C\n",
    "\n",
    "byte xtime(byte x)\n",
    "{\n",
    "    byte rv = (x<<1);\n",
    "    if (x & 0x80) {\n",
    "        rv ^= 0x1b;\n",
    "    } else {\n",
    "        asm volatile(\"nop\\n\\t\"\n",
    "             \"nop\\n\\t\"\n",
    "             \"nop\\n\\t\"::);\n",
    "    }\n",
    "    return rv;\n",
    "}\n",
    "\n",
    "\n",
    "void mixColumns(void)\n",
    "{\n",
    "    byte i, a, b, c, d, e;\n",
    "    \n",
    "    for(i = 0; i < 16; i+=4)\n",
    "    {\n",
    "        a = state[i]; b = state[i+1]; c = state[i+2]; d = state[i+3];\n",
    "        e = a ^ b ^ c ^ d;\n",
    "        state[i]   ^= e ^ xtime(a^b);\n",
    "        state[i+1] ^= e ^ xtime(b^c);\n",
    "        state[i+2] ^= e ^ xtime(c^d);\n",
    "        state[i+3] ^= e ^ xtime(d^a);\n",
    "    }\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an example, let's say we want to target the values `xtime(a^b)`, `xtime(b^c)`, `xtime(c^d)` and `xtime(d^a)` computed during the `MixColumns` operation. \n",
    "\n",
    "For this, we can define a reverse selection function that computes and returns these values.\n",
    "\n",
    "To implement such function we need to:\n",
    "\n",
    "- Create an empty array of shape `[nb traces, 16]` to store the values.\n",
    "- Compute the output of the `ShiftRows` operation.\n",
    "- For each column, compute the output of `xtime` for the pairs of consecutive bytes.\n",
    "- Store the values in the results array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scared.aes as aes\n",
    "\n",
    "def xtime(x):\n",
    "    rv = (x << 1) & 0xFF\n",
    "    if x & 0x80:\n",
    "        rv ^= 0x1b\n",
    "    return rv\n",
    "\n",
    "@scared.reverse_selection_function\n",
    "def custom_xtime(plaintext, key):\n",
    "\n",
    "    # Create an empty array of shape [nb traces, 16] to store the values.\n",
    "    values = np.zeros(shape=(plaintext.shape[0], 16), dtype=np.uint8)\n",
    "\n",
    "    # Compute the output of the ShiftRows operation.\n",
    "    sr = aes.encrypt(plaintext, key, at_round=1, after_step=aes.Steps.SHIFT_ROWS)\n",
    "\n",
    "    for column_index in range(4): # For each column\n",
    "        column = sr[:, range(column_index, column_index + 4)]\n",
    "\n",
    "        for byte_index in range(4):\n",
    "\n",
    "            # Compute the output of xtime for the pairs of consecutive bytes.\n",
    "            v = np.vectorize(xtime)(np.bitwise_xor(column[:, byte_index], column[:, (byte_index + 1) % 4]))\n",
    "\n",
    "            # Store the values in the results array.\n",
    "            values[:, column_index * 4 + byte_index] = v\n",
    "\n",
    "    return values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once defined, the reverse selection function can be used to compute the targeted values from the `THS` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = custom_xtime(**ths.metadatas)\n",
    "values.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function can be passed to other `scared` APIs, for instance to the reverse analysis API to perform reverse analysis on the data defined by the function."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
