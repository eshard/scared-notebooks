import os
import codecs
import hashlib
import requests
from tqdm.notebook import tqdm

url = "https://docs.google.com/uc?export=download"

dataset_id = {"dpa_v2.ets": "1DKQKYq7nDa_HgP3u6gYeXXKSTHvdIfSX"}  # Google Drive id
dataset_hash = {"dpa_v2.ets": "85cf01463ebd0eb7d854772493991ace5a7ae1010639a21b7b17f7cf88543f84"}
dataset_size = {"dpa_v2.ets": 131810037}


def download_file_from_google_drive(dataset, destination):
    id = dataset_id[dataset]
    session = requests.Session()
    response = session.get(url, params={'id': id}, stream=True)
    token = get_confirm_token(response)

    if token:  # if file too large, need to request a second time
        params = {'id': id, 'confirm': token}
        response = session.get(url, params=params, stream=True)

    save_response_content(response, destination, filesize=dataset_size[dataset])    


def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value
    return None


def save_response_content(response, destination, filesize):
    chunk_size = 10240
    with open(destination, 'wb') as f:
        for chunk in tqdm(response.iter_content(chunk_size), total=filesize // chunk_size,
                          unit='kB', unit_scale=False, leave=True):
            if chunk:
                f.write(chunk)


def check_sha256_hash(filepath, hash_string):
    h = hashlib.sha256()
    with open(filepath, mode='rb') as f:
        h.update(f.read())
    d = h.digest()
    return d == codecs.decode(hash_string, 'hex')


def download_dataset(dataset="dpa_v2.ets"):
    os.makedirs('./traces', exist_ok=True)
    print(f"##### Downloading {dataset} #####")

    filename = os.path.join('./traces/', dataset)
    if os.path.exists(filename):
        print(f"{filename} already exists")
    else:
        # Download dataset from Google Drive using curl
        print(f"Download {dataset} from Google Drive \n")
        download_file_from_google_drive(dataset=dataset, destination=filename)
        print(f"\nDownload of {filename} complete.")

    # Check the sha256 checksum
    print("Verifying checksum now")
    if check_sha256_hash(filename, dataset_hash[dataset]):
        print("Checksum OK \n")
    else:
        print("Checksum NOT OK! Removing file. Try to re-download file \n")
        os.remove(filename)
