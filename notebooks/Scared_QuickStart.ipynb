{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='./images/eshard.png' style='width: 180px; float:right;'>\n",
    "\n",
    "# `scared` Quick Start\n",
    "\n",
    "---\n",
    "\n",
    "* **Scared library notebooks**\n",
    "* Classification: __PUBLIC__\n",
    "* _CATEGORY_: Tutorial\n",
    "\n",
    "<img src='./images/start.jpg' style='width: 250px;'>\n",
    "\n",
    "In this notebook we present how to apply a Correlation Power Analysis (CPA) on an AES dataset using the `scared` library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scared"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The dataset\n",
    "----\n",
    "\n",
    "In this example, we will use the dataset from the StageGate #1 challenge of the [CHES 2016 CTF](http://ctf.newae.com/flags/).\n",
    "\n",
    "With `scared`, datasets can be loaded and manipulated using the `TraceHeaderSet` abstract reader API:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ths = scared.traces.read_ths_from_ets_file('./traces/stagegate1.ets')\n",
    "\n",
    "print(len(ths))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dataset is composed of 1,000 traces.\n",
    "\n",
    "Traces and metadata (plaintexts, ciphertexts etc) are accessible through the `THS` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traces = ths[0:100].samples[:]  # get the first 100 traces as numpy array\n",
    "plaintexts = ths[0:100].plaintext  # get first 100 plaintexts as numpy array\n",
    "\n",
    "print(traces.shape)\n",
    "print(plaintexts.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we plot the first trace of the dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(12, 4))\n",
    "ax.plot(traces[0])\n",
    "ax.set_xlabel('Time samples', fontsize=14)\n",
    "ax.set_title('First trace of the dataset - Beginning of the AES execution')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Attack\n",
    "----\n",
    "\n",
    "### Define target of the attack\n",
    "\n",
    "We need to choose an attack selection function to define the target of the attack. To attack the AES algorithm, the main pre-defined attack selection functions are:\n",
    "\n",
    "```python\n",
    "scared.aes.selection_functions.encrypt.FirstAddRoundKey\n",
    "scared.aes.selection_functions.encrypt.FirstSubBytes\n",
    "scared.aes.selection_functions.encrypt.LastSubBytes\n",
    "scared.aes.selection_functions.encrypt.DeltaRLastRounds\n",
    "```\n",
    "\n",
    "Here we will target the first round `SubBytes` output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "selection_function = scared.aes.selection_functions.encrypt.FirstSubBytes()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create attack object\n",
    "\n",
    "In this example we will perform a Hamming weight CPA. Below we define the attack object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "att = scared.CPAAttack(selection_function=selection_function,\n",
    "                       model=scared.HammingWeight(), \n",
    "                       discriminant=scared.maxabs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run attack\n",
    "\n",
    "To run the attack, we first need to create a container encapsulating our dataset. \n",
    "Here we also define an attack frame to focus the attack on the samples in the window `[0, 3000]`.\n",
    "\n",
    "Once this is done, we can run the CPA attack calling the `.run()` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "container = scared.Container(ths, frame=range(0, 3000))\n",
    "\n",
    "att.run(container)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Results\n",
    "\n",
    "Results of the attack are stored in `att.results` as a `numpy` array of shape `[nb guesses x nb targeted bytes x nb samples]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(att.results.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`att.scores` stores the best correlation obtained for each guess and for each targeted byte."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(att.scores.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can get the key recovered by the attack by selecting the guesses with the highest scores:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "recovered_masterkey = np.argmax(att.scores, axis=0).astype('uint8')\n",
    "\n",
    "print(recovered_masterkey)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check that the key is correct by encrypting the dataset's plaintexts with the recovered key:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "recomputed_ciphertexts = scared.aes.encrypt(ths.plaintext, recovered_masterkey)\n",
    "\n",
    "np.array_equal(recomputed_ciphertexts, ths.ciphertext)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key recovered is correct!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting results\n",
    "\n",
    "Below we display the results of the attack for the 16 targeted bytes using an interactive widget plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import interact\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "@interact(target_byte=range(16))\n",
    "def plot_byte_result(target_byte):\n",
    "    res_byte = att.results[:, target_byte, :]\n",
    "    fig, ax = plt.subplots(figsize=(8, 4))\n",
    "    ax.plot(res_byte.T, 'lightgrey')\n",
    "    ax.plot(res_byte[recovered_masterkey[target_byte]], 'red')  # plot the good guess correlation in red\n",
    "    ax.set_xlabel('Samples', fontsize=14)\n",
    "    ax.set_ylabel('Correlation', fontsize=14)\n",
    "    ax.legend(handles=[ax.lines[0], ax.lines[-1]], labels=['Wrong guesses', 'Good guess'], loc=\"upper left\")\n",
    "    plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
